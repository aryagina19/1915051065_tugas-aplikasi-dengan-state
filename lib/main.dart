import 'package:flutter/material.dart';

void main() {
  runApp(LuasSegitiga());
}

class LuasSegitiga extends StatefulWidget {
  @override
  _LuasSegitigaState createState() => _LuasSegitigaState();
}

class _LuasSegitigaState extends State<LuasSegitiga> {
  double alas = 0.0;
  double tinggi = 0.0;
  double luas = 0.0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Menghitung Luas Segitiga"),
            Row(
              children: [
                Text("Alas"),
                Expanded(
                    child: TextField(
                      onChanged: (txt) {
                        setState(() {
                          alas = double.parse(txt);
                        });
                      },
                      keyboardType: TextInputType.number,
                      maxLength: 3,
                    ))
              ],
            ),
            Row(
              children: [
                Text("Tinggi"),
                Expanded(
                  child: TextField(
                    onChanged: (txt) {
                      setState(() {
                        tinggi = double.parse(txt);
                      });
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                  ),
                )
              ],
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  luas = alas*tinggi/2;
                });
              },
              child: Text("Mulai Hitung!"),
            ),
            Text("Luas Dari Segitiga dengan Alas = $alas, dan Tinggi = $tinggi"),
            Text("Memiliki Luas = $luas")
          ],
        ),
      ),
    );
  }
}
